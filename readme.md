# Django Channels example: Shared whiteboard
This is a two-person (or more) shared chalkboard/whiteboard, where there is a canvas that both peoplecan draw on, each person’s stokes are visible in a different color.

### Requirements
- Python 3.6+
- Django 2.2+


### How to install
Create a virtual enviroment  and install dependencies.

```
virtualenv -p python3.6 whiteboard_env

source whiteboard_env/bin/activate

pip install -r requirements.txt
```

### Migrate database
```./manage.py migrate ```

### Run development server
```./manage.py runserver 0:8000```

### Run docker for Channel layer
```docker run -p 6379:6379 -d redis:2.8```

### Run tests (use pytest for testing Websockets)
```pytest whiteboard/tests.py ```

### Get test coverage report
```pytest whiteboard/tests.py --cov=whiteboard --cov-report=html ```

