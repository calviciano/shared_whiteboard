import json
import random

from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncJsonWebsocketConsumer

from .models import DrawingStroke


class DrawingConsumer(AsyncJsonWebsocketConsumer):
    async def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'drawing_%s' % self.room_name

        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )
        await self.accept()
        data = await self.get_drawing_data(self.room_name)

        # Get used color list
        used_color_list = []

        for session in data:
            used_color_list.append(session.color)
            # Send session to WebSocket
            await self.channel_layer.group_send(
                self.room_group_name,
                    {
                        'type': 'drawing_message',
                        'message': json.loads(session.data),
                        'color': session.color
                    }
            )
        # Check what color should use a new client
        new_color = self.get_available_color(used_color_list)

        # Send color to new client
        await self.channel_layer.group_send(
        self.room_group_name,
            {
                'type': 'setting_color',
                'color': new_color
            }
        )

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    async def receive_json(self, content):
        # Send message to room group
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'drawing_message',
                'message': content['data'],
                'color': content['color']
            }
        )
        # Save to database
        await self.save_stroke(
            content['data'],
            content['color'],
            self.room_name
        )


    async def drawing_message(self, event):
        """ Handle drawing messages """
        await self.send_json(
            {
                'data': event['message']
            }
        )

    async def setting_color(self, event):
        """ Handle cliente color set """
        await self.send_json(
            {
                'color_set': event['color']
            }
        )


    @database_sync_to_async
    def save_stroke(self, data, color, room_name):
        """ Saves a incoming drawing stroke to database """
        DrawingStroke.objects.create(
            data=json.dumps(data),
            room_name=room_name,
            color=color
        )

    @database_sync_to_async
    def get_drawing_data(self, room_name):
        """ Returns drawing data from a room_name """
        drawing_sessions = DrawingStroke.objects.filter(room_name=room_name)
        return drawing_sessions


    def get_available_color(self, used_colors):
        """ Get first unused color """
        colors = set([
            "blue", "red", "yellow",
            "green", "black", "cyan"
        ])

        if colors.difference(used_colors):
            new_color = colors.difference(used_colors).pop()
        else:
            new_color = random.choice(list(colors))

        return new_color