import random
import string

from django.shortcuts import render
from django.utils.safestring import mark_safe

from .models import DrawingStroke


def index(request):
    """ Homepage """
    # Create a random string
    random_room_name = ''.join(random.choices(string.ascii_uppercase + string.digits, k=6))
    used_room_names = DrawingStroke.objects.values_list('room_name', flat=True).distinct()
    context = {
        'room_name': random_room_name,
        'used_room_names': used_room_names,
    }
    return render(request, 'whiteboard/index.html', context)


def room(request, room_name):
    """ Room view """
    return render(request, 'whiteboard/room.html', {
        'room_name': room_name
    })
