from django.db import models


class DrawingStroke(models.Model):
    """ Represents a drawing stroke from a user """

    data = models.TextField("Drawing data", blank=True, null=True)
    room_name = models.TextField("Room name")
    created = models.DateTimeField("Creation date", auto_now_add=True)
    color = models.CharField("Color", max_length=50, default='blue')

    class Meta:
        verbose_name = 'DrawingStroke'
        verbose_name_plural = 'DrawingStrokes'

    def __str__(self):
        return str(self.id)
