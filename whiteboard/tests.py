import pytest

from django.test import TestCase

from django.conf.urls import url
from channels.testing import WebsocketCommunicator
from channels.routing import URLRouter

from .consumers import DrawingConsumer
from .models import DrawingStroke


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_drawing_consumer():

    json_data = {"data": [{"x": 1543, "y": 461}], "color": "blue"}

    application = URLRouter([
      url(r'ws/(?P<room_name>\w+)/$', DrawingConsumer),
    ])
    communicator = WebsocketCommunicator(application, "/ws/test/")
    connected, subprotocol = await communicator.connect()
    assert connected
    # Test sending text
    await communicator.send_json_to(json_data)
    response = await communicator.receive_from()
    assert response
    # Close
    await communicator.disconnect()


class DrawingTestCase(TestCase):
    def setUp(self):
        self.test_data = '{"data": [{"x": 1543, "y": 461}], "color": "blue"}"'
        DrawingStroke.objects.create(data=self.test_data, room_name="testing")

    def test_stroke_save_and_retrieve(self):
        """Can save and retrieve data by room_name."""
        testing_room = DrawingStroke.objects.filter(room_name="testing").last()
        # Test saved data
        self.assertEqual(testing_room.data, self.test_data)
        # Test str
        self.assertEqual(str(testing_room), str(testing_room.id))

    def test_index(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'whiteboard/index.html')

    def test_room_name(self):
        response = self.client.get('/testing_room/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'whiteboard/room.html')