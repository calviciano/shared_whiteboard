(function () {

    /* Canvas */
    var canvas = document.getElementById('drawCanvas')
    var ctx = canvas.getContext('2d')
    var color = "red"
    var user_color = ""
    var history = []

    canvas.width = Math.min(document.documentElement.clientWidth, window.innerWidth || 300)
    canvas.height = Math.min(document.documentElement.clientHeight, window.innerHeight || 300)

    ctx.strokeStyle = color
    ctx.lineWidth = '3'
    ctx.lineCap = ctx.lineJoin = 'round'

    /* Mouse and touch events */
    var isTouchSupported = 'ontouchstart' in window
    var isPointerSupported = navigator.pointerEnabled
    var isMSPointerSupported = navigator.msPointerEnabled;

    var downEvent = isTouchSupported ? 'touchstart' : (isPointerSupported ? 'pointerdown' : (isMSPointerSupported ? 'MSPointerDown' : 'mousedown'))
    var moveEvent = isTouchSupported ? 'touchmove' : (isPointerSupported ? 'pointermove' : (isMSPointerSupported ? 'MSPointerMove' : 'mousemove'))
    var upEvent = isTouchSupported ? 'touchend' : (isPointerSupported ? 'pointerup' : (isMSPointerSupported ? 'MSPointerUp' : 'mouseup'))

    canvas.addEventListener(downEvent, startDraw, false)
    canvas.addEventListener(moveEvent, draw, false)
    canvas.addEventListener(upEvent, endDraw, false)

    /* Websocket */
    var whiteboardSocket = new WebSocket(
        'ws://' + window.location.host +
        '/ws/' + roomName + '/');

    whiteboardSocket.onmessage = function (e) {
        var data = JSON.parse(e.data)
        var message = data['data']
        if ('color_set' in data && user_color == ""){
            // Set drawing color
            user_color = data['color_set']
            color = user_color
        }

        drawFromStream(message)
    };

    whiteboardSocket.onclose = function (e) {
        console.error(' == Socket closed')
    };

    function publish(data) {
        whiteboardSocket.send(JSON.stringify(
            {
            'data': data,
            'color': color
            }
        ))
     }

    /* Drawings */
    function drawOnCanvas(color, plots) {
        ctx.strokeStyle = color;
        ctx.beginPath()
        ctx.moveTo(plots[0].x, plots[0].y)
        history.push(plots)

        for (var i = 1; i < plots.length; i++) {
            ctx.lineTo(plots[i].x, plots[i].y)
        }
        ctx.stroke()

    }

    function drawFromStream(message) {
        if (!message || message.plots.length < 1) return;
        drawOnCanvas(message.color, message.plots)
    }

    var isActive = false;
    var plots = [];

    function draw(e) {
        if (!isActive) return;

        var x = isTouchSupported ? (e.targetTouches[0].pageX - canvas.offsetLeft) : (e.offsetX || e.layerX - canvas.offsetLeft)
        var y = isTouchSupported ? (e.targetTouches[0].pageY - canvas.offsetTop) : (e.offsetY || e.layerY - canvas.offsetTop)

        var data = { x: (x << 0), y: (y << 0) }
        plots.push(data) // round numbers for touch screens

        drawOnCanvas(color, plots)
    }

    function startDraw(e) {
        e.preventDefault()
        isActive = true
    }

    function endDraw(e) {
        e.preventDefault()
        isActive = false

        publish({
            plots: plots,
            color: color
        })
        plots = []
    }
})()
