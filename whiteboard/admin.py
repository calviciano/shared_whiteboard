from django.contrib import admin
from .models import DrawingStroke

class DrawingStrokeAdmin(admin.ModelAdmin):
    pass


admin.site.register(DrawingStroke, DrawingStrokeAdmin)